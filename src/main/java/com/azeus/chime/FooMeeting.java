package com.azeus.chime;

import com.amazonaws.services.chime.model.Meeting;

import org.springframework.data.annotation.Id;

public class FooMeeting {
  @Id
  private String meetingName;

  private Meeting meeting;

  public FooMeeting(final String meetingName, final Meeting meeting) {
    this.meetingName = meetingName;
    this.meeting = meeting;
  }

  public String getMeetingName() {
    return meetingName;
  }

  public void setMeetingName(String meetingName) {
    this.meetingName = meetingName;
  }

  public Meeting getMeeting() {
    return meeting;
  }

  public void setMeeting(Meeting meeting) {
    this.meeting = meeting;
  }
}
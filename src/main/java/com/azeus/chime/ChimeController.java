package com.azeus.chime;

import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.chime.AmazonChime;
import com.amazonaws.services.chime.AmazonChimeClientBuilder;
import com.amazonaws.services.chime.model.Attendee;
import com.amazonaws.services.chime.model.CreateAttendeeRequest;
import com.amazonaws.services.chime.model.CreateMeetingRequest;
import com.amazonaws.services.chime.model.Meeting;


@RestController
public class ChimeController {
	private AmazonChime chime;

	@Autowired
	FooMeetingRepository repository;
	
	@PostConstruct
	public void initialize(){
		BasicAWSCredentials creds = new BasicAWSCredentials("AKIAWV73QKXMUPTROYG5","OG2vHTB71IqGoxQQW177txumsTSMNrcpNqwLREU+");
		AWSStaticCredentialsProvider credsProvider = new AWSStaticCredentialsProvider(creds); 
		chime =  AmazonChimeClientBuilder.standard().withCredentials(credsProvider).withRegion(Regions.US_EAST_1).build();
	}
	
	@PostMapping("join")
	public ResponseEntity<Map<String,Object>> createMeeting(@RequestParam String meetingId, @RequestParam String name) {
		Meeting meeting = startMeeting(meetingId);
						
		CreateAttendeeRequest createAttendeeReq = new CreateAttendeeRequest();
		createAttendeeReq.setMeetingId(meeting.getMeetingId());
		createAttendeeReq.setExternalUserId(name);
		Attendee attendee = chime.createAttendee(createAttendeeReq).getAttendee();
		
		Map<String,Object> info = new HashMap<String,Object>();
		info.put("Meeting", meeting);
		info.put("Attendee",attendee);
		Map<String,Object> joinInfo = new HashMap<String,Object>();
		joinInfo.put("JoinInfo", info);
		return ResponseEntity.ok(joinInfo);
	}

	/**
	 * Retrieve stored meeting or create a new meeting
	 * @param meetingId
	 * @return
	 */
	private Meeting startMeeting(final String meetingId) {
		Optional<FooMeeting> fooMeeting = repository.findById(meetingId);
		Meeting meeting;
		if (fooMeeting.isPresent()) {
			meeting = fooMeeting.get().getMeeting();
		} else {
			CreateMeetingRequest createMeetingReq = new CreateMeetingRequest();
			createMeetingReq.setClientRequestToken(UUID.randomUUID().toString());
			createMeetingReq.setExternalMeetingId(meetingId);
			createMeetingReq.setMediaRegion("ap-southeast-1");
			meeting = chime.createMeeting(createMeetingReq).getMeeting();
			repository.save(new FooMeeting(meetingId, meeting));
		}
		return meeting;
	}
}

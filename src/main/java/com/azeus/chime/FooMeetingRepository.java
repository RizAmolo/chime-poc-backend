package com.azeus.chime;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface FooMeetingRepository extends MongoRepository<FooMeeting, String> {
}